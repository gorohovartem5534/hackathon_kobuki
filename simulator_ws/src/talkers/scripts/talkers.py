#!/usr/bin/env python
# license removed for brevity
import rospy
#from std_msgs.msg import String
from geometry_msgs.msg import Twist
def talker():
    pub = rospy.Publisher('turtle1/cmd_vel', Twist, queue_size=10)
    rospy.init_node('talker', anonymous=True)
    rate = rospy.Rate(10) # 10hz
    while not rospy.is_shutdown():
        #hello_str = "hello world %s" % rospy.get_time() #TODO
	hello_str = Twist()
	hello_str.linear.x = 1.0
	hello_str.angular.z = 1.0
        rospy.loginfo(hello_str)
        pub.publish(hello_str)
        rate.sleep()

if __name__ == '__main__':
    try:
        talker()
    except rospy.ROSInterruptException:
        pass
