#!/usr/bin/env python

import rospy
import cv2
import numpy as np
from cv_bridge import CvBridge, CvBridgeError
from sensor_msgs.msg import Image

class image_converter:

  def __init__(self):
    self.image_pub = rospy.Publisher("image_topic_2",Image, queue_size = 1)

    self.bridge = CvBridge()
    self.image_sub = rospy.Subscriber("usb_cam/image_raw",Image,self.callback)

  def callback(self,data):
    try:
      cv_image = self.bridge.imgmsg_to_cv2(data, "bgr8")
    except CvBridgeError as e:
      print(e)

    (rows,cols,channels) = cv_image.shape
    if cols > 60 and rows > 60 :
      cv2.circle(cv_image, (50,50), 10, 255)
    low_green = np.array((75, 20, 20), np.uint8)
    high_green = np.array((170, 100, 100), np.uint8)
    img_hsv = cv2.cvtColor(cv_image, cv2.COLOR_BGR2HSV)
    mask_green = cv2.inRange(img_hsv, low_green, high_green)

    cv2.imshow("Image window", mask_green)
    cv2.waitKey(3)

    try:
      self.image_pub.publish(self.bridge.cv2_to_imgmsg(cv_image, "bgr8"))
    except CvBridgeError as e:
      print(e)

if __name__ == '__main__':
    try:
	rospy.init_node("cv_bridge_test")
	image_converter()
	rospy.spin()
    except KeyboardInterrupt:
	cv2.destroyAllWindows()

